import { Map } from "immutable";

const reducer = function (state = Map(), action) {
    switch (action.type) {
        case "SET_STATE":
            return state.merge(action.state);
        case "SHOW_GAMES":
            let newGames;
            return state.update("games", function (games) {
                games = action.showGames;
                newGames = games;
                return games;
            }).update("pageCount", function (pageCount) {
                let pages = Math.ceil(newGames.length / state.get("maxGamesOnPage"));
                pageCount = pages;
                return pages;
            });
        case "PAGE_CHANGE_TO":
            return state.update("currentPage", function () {
                if (action.index > state.get("pageCount")) {
                    action.index = state.get("pageCount")
                }
                else if (action.index < 1) {
                    action.index = 1
                }
                return action.index;

            })
        case "SET_CURRENT_GAME":
            return state.update("currentGame",function(game){
                return action.game;
            })    
    }
    return state;
}
export default reducer;