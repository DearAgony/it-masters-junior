const showGames = function (showGames) {
    
    return {
        type: "SHOW_GAMES",
        showGames
    };
};
const pageChangeTo = function(index){
    return {
        type: "PAGE_CHANGE_TO",
        index
    }
}
const setCurrentGame = function(game){
    return {
        type: "SET_CURRENT_GAME",
        game
    }
}
export default {
    showGames,
    pageChangeTo, 
    setCurrentGame, 
};