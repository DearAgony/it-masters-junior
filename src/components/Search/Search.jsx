import React from "react";
import "./style.css"
import { Button } from "../Button/Button";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";
import { serverAPI } from "../../serverAPI";


class Search extends React.Component {
    inputRef=React.createRef();
    showGames = () => {
        let searchInput = this.inputRef.current.value;
        let newGames = new serverAPI();
        let showGamesPromise = newGames.getGames(searchInput);
        showGamesPromise.then(data => this.props.showGames(data));
    }
    render() {
        return <div className="gameInput">
            <input type="text" placeholder="Введите название" ref={this.inputRef} />
            <Button title="Seach" onClick={this.showGames} />
        </div>
    }
}


function mapStateToProps(state) {
    return {
        games: state.get("games"),
        inputValue: state.get("InputValue")
    }
}

export default connect(mapStateToProps, Actions)(Search);