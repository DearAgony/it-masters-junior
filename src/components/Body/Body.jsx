import React, { Fragment } from "react";
import GameList from "../GameList/GameList";
import Search from "../Search/Search";
import GameInfo from "../GameInfo/GameInfo";
import { ConnectedPagination as Pagination } from "../Pagination";
import { Switch, Route} from "react-router-dom";


class Body extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <Switch>
            <Route exact path="/">
                <Search />
                <GameList />
                <Pagination />
            </Route>
            <Route path="/Game/:id" component={GameInfo}/>   
        </Switch>;

    }

}


export default Body;
