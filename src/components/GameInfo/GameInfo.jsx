import React from "react";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";


class GameInfo extends React.Component {
    render() {
        const releaseDateInMs=this.props.game.first_release_date*1000;
        return <div className="gameInfo">
            <h3>{this.props.game.name}</h3>
            <a href=""><img src={this.props.game.cover.url}/></a>
            <p>Game Raiting: {Math.floor(this.props.game.rating*100)/100}</p>
            <p>Release Date: {new Date(releaseDateInMs).toDateString()}</p>
            <p>Discription: <br/>{this.props.game.summary}</p>
            <p>Game WebSite: <a href={this.props.game.url}>{this.props.game.url}</a></p>
            
        </div>

    };
}
function mapStateToProps(state) {
    return  {
        game: state.get("currentGame"),
    }
}

export default connect(mapStateToProps, Actions)(GameInfo);