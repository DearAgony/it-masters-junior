
import React, { Fragment } from "react";
import Body from "../Body/Body";
import { Header } from "../Header/Header";
import { Provider } from "react-redux";
import { createStore } from "redux";
import GameListReducer from "../../Redux/Reducer";
import { serverAPI } from "../../serverAPI";
import { BrowserRouter } from "react-router-dom";



const store = createStore(GameListReducer);
store.dispatch({
    type: "SET_STATE",
    state: {
        games: [],
        inputValue: "",
        pageCount: 1,
        currentPage: 1,
        maxGamesOnPage: 10,
        currentGame:{
            id:"",
            cover:{
                url:"",
            },
            name:"Loading",
            rating:"",
            category:"",
            first_release_date:1,
            summary:"",
            url:"",



        },
    }
});
new serverAPI().topGames().then(data => {
    store.dispatch({
        type: "SET_STATE",
        state: {
            games: data,
        }
    });
});


const App = (props) => <Provider store={store}>
    <BrowserRouter>
        <Header />
        <Body />
    </BrowserRouter>
</Provider>;

export {
    App
} 