import React from "react";
import {connect} from "react-redux";
import Actions from "../../Redux/Actions";
import { Game } from "../Game";
import "./style.css";


class GameList extends React.Component{
       
    render(){
        return <div className="gameList">
            {this.props.games.map(game=><Game 
            key ={game.id}
            name={game.name}
            id={game.id}
            cover={game.cover ? game.cover.url : null}/>)
        }
            
        </div>
    }
}

function mapStateToProps(state) {
    let take = state.get("maxGamesOnPage");
    let skip = (state.get("currentPage")-1)*take;
    let gamesLeft = state.get("games").slice(skip, skip+take);
    let pages = Math.ceil(state.get("games").length/state.get("maxGamesOnPage"))
    return {
        games: gamesLeft,
        pageCount: pages

     }
    
}

export default connect(mapStateToProps, Actions)(GameList);