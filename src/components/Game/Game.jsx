import React from "react";
import "./style.css";
import { withRouter } from "react-router-dom";
import { serverAPI } from "../../serverAPI";
import { connect } from "react-redux";
import Actions from "../../Redux/Actions";

class Game extends React.Component {
    onClick = (e) => {
        e.preventDefault();
        this.props.history.push(`/Game/${this.props.id}`);
        let choosenGame = new serverAPI();
        let choosenGamPromise = choosenGame.choosenGame(this.props.id);
        choosenGamPromise.then(data => {
            this.props.setCurrentGame(data[0]);
        });



    }
    render() {
        return <a className="game" href="#" onClick={this.onClick}>
            <img src={this.props.cover}  alt="нету картинки"/>
            <p>{this.props.name}</p>
        </a>

    };
} 
function mapStateToProps(state) {
    return {
        currentGame: state.get("currentGame"),
    }
}
const GameWithRouter = withRouter(Game);
const GameWithRouterAndConnect  = connect(mapStateToProps, Actions)(GameWithRouter)
export {
    GameWithRouterAndConnect as Game
}

