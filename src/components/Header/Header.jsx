import React, { PureComponent } from "react";
import "./style.css";


class Header extends React.PureComponent{
    render(){
        return <div className="header">
            <h2> Web App</h2>
        </div>
    }
}

export{
    Header
}