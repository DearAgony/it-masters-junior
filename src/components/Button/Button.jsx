import React from "react";

const Button = (props) => <button className="seachButton" onClick={props.onClick}>{props.title}</button>;

export{
    Button
}