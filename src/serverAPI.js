class serverAPI {
    async getGames(searchInput) {
        let gameResponce = await fetch('https://cors-anywhere.herokuapp.com/https://api-v3.igdb.com/games', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'user-key': 'a7dc33f27c16616a34c2bc2366099996',
            },
            body: `fields id, name, cover.url; search "${searchInput}"; limit 300;`
        })
        let response = await gameResponce;
        let value = await response.json();
        return value
    }
    async topGames() {
        let gameResponce = await fetch('https://cors-anywhere.herokuapp.com/https://api-v3.igdb.com/games', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'user-key': 'a7dc33f27c16616a34c2bc2366099996',
            },
            body: `fields id, name, cover.url, rating;where rating>0;sort rating desc;`
        })
        let response = await gameResponce;
        let value = await response.json();
        return value
    }
    async choosenGame(id) {
        let gameResponce = await fetch('https://cors-anywhere.herokuapp.com/https://api-v3.igdb.com/games', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'user-key': 'a7dc33f27c16616a34c2bc2366099996',
            },
            body: `fields *, cover.url; where id =${id};`
        })
       
        let response = await gameResponce;
        let value = await response.json(); 
        return value
    }
}

export {
    serverAPI
}